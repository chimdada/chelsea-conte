# frozen_string_literal: true
require "aws-sdk"
    class AwsUtil
      def initialize
        @bucket_name =  "pixtavietnam-common"
      end

      def s3_object(key, path)
        s3_client.get_object(response_target: path,
                             bucket: @bucket_name,
                             key: key)
      end

      def s3_put_object(file, key)
        s3_client.put_object(body: file,
                             bucket: @bucket_name, key: key)
      end

      def sqs_adapter
        @sqs_adapter ||= Pixta::Core::SqsAdapter.new(queue_name: Pixta::Core::Config.queue_name)
      end

      def cloudsearch_client
        @cloudsearch_client ||= Aws::CloudSearchDomain::Client.new(endpoint: ENV["ENDPOINT"])
      end

      private

      def s3_client
        @s3_client ||= Aws::S3::Client.new
      end
    end
