require "./bowling.rb"
require "./sqs_adapter.rb"

RSpec.describe Bowling, "#score" do
  let(:sqs_adapter) { SqsAdapter.new(queue_name: "chelsea_channel_test") }


  before(:all) do
    # upload master data to  s3

  end

  after(:each) do
    sqs_adapter.purge_queue!
  end

  context "with no strikes or spares" do
    it "sums the pin count for each roll" do
      bowling = Bowling.new
      20.times { bowling.hit(4) }
      expect(bowling.score).to eq 80
    end
  end

  #context "with message" do
  #  describe "#count available messages" do
  #    before do
  #      sqs_adapter.send_message("welcome back chelsea")
  #    end
  #    it { expect(sqs_adapter.appr_number_of_messages).to eq 1 }
  #  end
  #end
  
  context "test s3" do
    describe "#download s3" do
       
    end
  end
end
