# frozen_string_literal: true
class SqsAdapter
  def initialize(sqs_client: Aws::SQS::Client.new, queue_name:)
    @sqs_client = sqs_client
    @queue_url = sqs_client.get_queue_url(queue_name: queue_name).queue_url
  end

  def send_message(message)
    sqs_client.send_message(
      queue_url: queue_url,
      message_body: message
    )
  end

  def delete_message(message)
    sqs_client.delete_message(
      queue_url: queue_url,
      receipt_handle: message.receipt_handle
    )
  end

  def delete_message_batch(messages)
    messages.each_slice(10) do |message_batch|
      sqs_client.delete_message_batch(
        queue_url: queue_url,
        entries: message_batch
      )
    end
  end

  def receive_message(options = {})
    sqs_client.receive_message(
      queue_url: queue_url,
      max_number_of_messages: options[:max_number_of_messages],
      visibility_timeout: options[:visibility_timeout],
      wait_time_seconds: options[:wait_time_seconds]
    )
  end

  def appr_number_of_messages
    sqs_client.get_queue_attributes(
      queue_url: queue_url,
      attribute_names: ["ApproximateNumberOfMessages"]
    ).attributes.values.first.to_i
  end

  def purge_queue!
    @sqs_client.purge_queue(queue_url: @queue_url)
  end

  def delete_queue
    @sqs_client.delete_queue(queue_url: @queue_url)
  end

  private

  attr_reader :sqs_client, :queue_url
end
